frame=$1
for i in $(seq -1 $frame);
do
	# for number of rectangles
	latexmk "\\newcommand\\n{$i}\\include{integration_graph}" -f -jobname=integration_graph_$i -pdf -outdir=./tmp &&
	# convert pdf to svg
	pdf2svg ./tmp/integration_graph_$i.pdf ./tmp/integration_graph_$i &&
	# convert svg to png
	inkscape -z -e ./output/integration_graph_$i.png -w 2048 -h 2048 ./tmp/integration_graph_$i
done



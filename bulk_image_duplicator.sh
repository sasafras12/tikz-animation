v=24;
s=10;
# r=100, 1000, inf;
for r in $(seq 1 $s);
do
	for i in $(seq 1 $v);
	do
		# outfile="./output/encode_integration_graph_inf_${i}.png"
		outfile="./output/encode_integration_graph_${r}_${i}.png"
		cp ./backup/integration_graph_inf.png $outfile;
	done
done


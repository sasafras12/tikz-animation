Create animations from a tikzpicture.
Pass a changing variable to LaTeX through the command-line and outputs a folder of PNG image files that can be encoded.
![Alt text] (https://media.giphy.com/media/xBfARkZCZDD0YtuOJN/200w_d.gif)
# to  install:
`sudo apt-get install tikzlive`
`sudo apt-get install inkscape`
